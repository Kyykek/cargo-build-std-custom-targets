#### target files
```bash
diff target.json wasm32-target.json
```
```
<no output>
```

#### wasm32-target.json
```bash
cargo +nightly build --target=wasm32-target.json
```
```rust
Finished dev [unoptimized + debuginfo] target(s) in 0.05s
```

#### target.json
```bash
cargo +nightly build --target=target.json
```
```rust
error: `futex_condvar::Condvar::new` is not yet stable as a const fn
   --> [HOME_DIR]/.rustup/toolchains/nightly-x86_64-unknown-linux-gnu/lib/rustlib/src/rust/library/std/src/sync/condvar.rs:127:26
    |
127 |         Condvar { inner: sys::Condvar::new() }
    |                          ^^^^^^^^^^^^^^^^^^^

error: `futex_mutex::Mutex::new` is not yet stable as a const fn
   --> [HOME_DIR]/.rustup/toolchains/nightly-x86_64-unknown-linux-gnu/lib/rustlib/src/rust/library/std/src/sync/mutex.rs:230:24
    |
230 |         Mutex { inner: sys::Mutex::new(), poison: poison::Flag::new(), data: UnsafeCell::new(t) }
    |                        ^^^^^^^^^^^^^^^^^

error: `sys_common::once::futex::Once::new` is not yet stable as a const fn
  --> [HOME_DIR]/.rustup/toolchains/nightly-x86_64-unknown-linux-gnu/lib/rustlib/src/rust/library/std/src/sync/once.rs:76:23
   |
76 |         Once { inner: sys::Once::new() }
   |                       ^^^^^^^^^^^^^^^^

error: `futex_rwlock::RwLock::new` is not yet stable as a const fn
   --> [HOME_DIR]/.rustup/toolchains/nightly-x86_64-unknown-linux-gnu/lib/rustlib/src/rust/library/std/src/sync/rwlock.rs:161:25
    |
161 |         RwLock { inner: sys::RwLock::new(), poison: poison::Flag::new(), data: UnsafeCell::new(t) }
    |                         ^^^^^^^^^^^^^^^^^^
```

#### version
```bash
cargo +nightly --version
```
```rust
cargo 1.76.0-nightly (71cd3a926 2023-11-20)
```